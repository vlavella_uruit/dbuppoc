﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using DbUp;

namespace DdUpPoc
{
	class Program
	{
		static int Main(string[] args){   

			var connectionString = ConfigurationManager.ConnectionStrings["DbUpPocConnection"].ConnectionString;
			
			EnsureDatabase.For.SqlDatabase(connectionString);
			
			var upgrader =
				DeployChanges.To
					.SqlDatabase(connectionString)
					.WithScriptsEmbeddedInAssembly(Assembly.GetExecutingAssembly())
					.WithTransaction()			
					.LogToConsole()
					.Build();

			Console.ForegroundColor = ConsoleColor.Blue;
			Console.WriteLine("Already executed scripts: " + upgrader.GetExecutedScripts().Count);
			Console.WriteLine("Pending scripts: " + upgrader.GetScriptsToExecute().Count);
			Console.ResetColor();

			var result = upgrader.PerformUpgrade();

			if (!result.Successful)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine(result.Error);
				Console.ResetColor();
				return -1;
			}

			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine("Success!");
			Console.ResetColor();
			return 0;
		}

	}
}
